package Exception;
import java.lang.reflect.Field;
/*
 * creer une class exception personnalise
 * � chaque fois	que	nous	catchons	une	excepDon	due	� l�acc�s � un champ,
 *journalisez	(log)	l�excepDon	en	signalant	le	contexte	de	mani�re claire
 */
public class ErreurAccesAuxChamps extends IllegalAccessException{
	
	//defini une varaible field , pour la jornalisation
	public Field f;
    
	/*
	 * consctructor
	 */
	public ErreurAccesAuxChamps(Field f) {
		super();
		this.f = f;
		f.setAccessible(true);
	}
	/*
	 * metode toString
	 */
	@Override
	public String toString() {
		return "il y a une erreur quand acceder la champ: '"+f.getName()+"' de type "+f.getType();
		
	}

}
