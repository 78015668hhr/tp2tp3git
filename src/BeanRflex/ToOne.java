package BeanRflex;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import annotation.Jointure;
import annotation.Table;
import annotation.VMignorer;
/*
 * creer une class ToMany qui presenter les information de inner bean
 */
public class ToOne {
	private BeanReflexRetrieve innerBeanReflex,outerBeanReflex;
	
	//defini deux variable de type field , qui est le outerbeanField,et innerbeanField
	//par exemple : si ici ToOne presenter le type etudiant
	//outerBeanField : etudiant , innerBeanField : List<Etudiant>
	private Field innerBeanField,outerBeanField;
	
	//defini une variable d'annotation "Jointure"
	private Jointure annoField;
	
	/*
	 * constructor
	 */
	public ToOne(BeanReflexRetrieve outerBeanReflex, Field outerBeanField) {
		this.outerBeanReflex = outerBeanReflex;
		this.outerBeanField = outerBeanField;
		this.annoField = outerBeanField.getAnnotation(Jointure.class);
	}
	/*
	 * creer une methode pour obtenir le requete sql pour retirer les inner bean
	 */
	public String getInnerSql() {
		return annoField.where();
	}
	/*
	 * une methode qui obtenir le innerBeanField et innerBeanField
	 */
	public BeanReflexRetrieve getInnerBeanReflex() throws NoSuchFieldException, SecurityException {
		
		//obtenir l'annotation "Table",de outerBean
		//par exemple ,ici toOne est etudiant, sa tableOuterBean peut etre inscription,
		//greace a cette variable , on peut identifier sa outerBean,et ajouter les control(eviter les redondants)
		Table tableOuterBean =outerBeanReflex.getBeanRep().getAnnotation(Table.class);
		
		//otenir le innerbean 
		BeanReflexRetrieve innerBeanrflx= new BeanReflexRetrieve(outerBeanField.getType(),tableOuterBean);
		

		//otenir le innerBeanField
		this.innerBeanField = innerBeanrflx.getBeanRep().getDeclaredField(outerBeanField.getAnnotation(VMignorer.class).innerBeanFieldName());
		
		this.innerBeanReflex = innerBeanrflx;
		
		return this.innerBeanReflex;
	}
	/*
	 * les getteur et setteur
	 */
	public Jointure getAnnoField() {
		return annoField;
	}
	public void setAnnoField(Jointure annoFeild) {
		this.annoField = annoFeild;
	}
	
	public BeanReflexRetrieve getOuterBeanReflex() {
		return outerBeanReflex;
	}
	public void setOuterBeanReflex(BeanReflexRetrieve outerBeanReflex) {
		this.outerBeanReflex = outerBeanReflex;
	}
	public Field getInnerBeanField() {
		return innerBeanField;
	}
	public void setInnerBeanField(Field innerBeanField) {
		this.innerBeanField = innerBeanField;
	}
	public Field getOuterBeanField() {
		return outerBeanField;
	}
	public void setOuterBeanField(Field outerBeanField) {
		this.outerBeanField = outerBeanField;
	}
	

	
	
	

}
