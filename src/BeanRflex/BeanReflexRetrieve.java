package BeanRflex;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import annotation.Table;
import annotation.VMignorer;
/*
 * creer une class BeanReflexRetrieve qui presenter tout les contenu d'une class de bean
 * grace a cette class, on peut ajouter bcp controls aux classes de bean
 */
public class BeanReflexRetrieve {
	
	//une variable de list qui contient les field d'une class de bean
	private List<Field> listField;
	
	//une variabe de list qui contient les object toOne(qui presenter les information de inner bean)
	private List<ToOne> listToOne;
	
	//une variabe de list qui contient les object toOne(qui presenter les information de inner list de bean)
	private List<ToMany>listToMany;
	
	//une varaible de representant d'une class de bean
	private Class<?> beanRep;
	
	//une variable de annotion "table",pour ajouter les controls aux classes de bean
	private Table annoTable;
	
	/*
	 * constructor
	 */
	public BeanReflexRetrieve(Class<?> beanRep,Table annoTable) {
		this.beanRep = beanRep;
		this.annoTable = annoTable;
		listField = new ArrayList<Field>(); 
		listToOne = new ArrayList<ToOne>();
		listToMany = new ArrayList<ToMany>();
		Field[] fields = beanRep.getDeclaredFields();
		
		//dans la boucle for,obtenir tous les field d'une class de bean et tout les Object qui represente 
		//les inner bean et inner liste de bean(ToOne ,ToMany)
		for(Field f : fields) {
			
			//si f.isAnnotationPresent(VMignorer.class) == true ,ca veut dire il y a des innerField de type complex
			if(f.isAnnotationPresent(VMignorer.class)) {
				
				//obtenir l'annotation "table" de field complex
				Table  annoDeTable= f.getAnnotation(VMignorer.class).type().getAnnotation(Table.class);
				
				//si f.getType() == List.class&&!annoTable.TableName().equals(annoDeTable.TableName()) == true
				//c'est a dire le field est une list de bean , et il n'est pas le meme class de this.BeanReflexRetrieve!!!!
				//(pour eviter generer les redondants!!!) 
			 if(f.getType() == List.class&&!annoTable.TableName().equals(annoDeTable.TableName()))
				listToMany.add(new ToMany(this,f));
			 
			    //si f.getType() != List.class&&!annoTable.TableName().equals(annoDeTable.TableName()) == true
				//c'est a dire le field est une bean , et il n'est pas le meme class de this.BeanReflexRetrieve!!!!
				//(pour eviter generer les redondants!!!)
			 else if(f.getType()!=List.class&&!annoTable.TableName().equals(annoDeTable.TableName())) 
				 listToOne.add(new ToOne(this,f));
			 
			 }//end outer if
			
			else
				listField.add(f);	
		}//end for	
		

	}


	/*
	 * les getteur et setteur
	 */

	public List<Field> getListField() {
		return listField;
	}



	public void setListField(List<Field> listField) {
		this.listField = listField;
	}



	public List<ToOne> getListToOne() {
		return listToOne;
	}



	public void setListToOne(List<ToOne> listToOne) {
		this.listToOne = listToOne;
	}



	public List<ToMany> getListToMany() {
		return listToMany;
	}



	public void setListToMany(List<ToMany> listToMany) {
		this.listToMany = listToMany;
	}



	public Class<?> getBeanRep() {
		return beanRep;
	}



	public void setBeanRep(Class<?> beanRep) {
		this.beanRep = beanRep;
	}



	public Table getAnnoTable() {
		return annoTable;
	}



	public void setAnnoTable(Table annoTable) {
		this.annoTable = annoTable;
	}
	



	



}
