package BeanRflex;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import annotation.Jointure;
import annotation.Table;
import annotation.VMignorer;
/*
 * creer une class ToMany qui presenter les information de inner liste de bean
 */
public class ToMany {
	
	BeanReflexRetrieve innerBeanReflex,outerBeanReflex;
	
	//defini deux variable de type field , qui est le outerbeanField,et innerbeanField
	//par exemple : si ici ToMany presenter le type Inscription
	//outerBeanField : List<Inscription>,innerBeanField : cours,etudiant
	Field innerBeanField,outerBeanField;
	
	//defini une variable d'annotation "Jointure"
	private Jointure annoField;
	
	/*
	 * constructor
	 */
	public ToMany(BeanReflexRetrieve outerBeanReflex, Field outerBeanField ) {
		this.outerBeanReflex = outerBeanReflex;
		this.outerBeanField = outerBeanField;
		this.annoField = outerBeanField.getAnnotation(Jointure.class);
	}
	/*
	 * une methode qui obtenir le innerBeanField et innerBeanField
	 */
	public BeanReflexRetrieve getInnerBeanReflex() throws NoSuchFieldException, SecurityException {
		
		//obtenir l'annotation "Table",de outerBean
		//par exemple ,ici toMany est Insription, sa tableOuterBean peut etre etudiant ou cours,
		//greace a cette variable , on peut identifier sa outerBean,et ajouter les control(eviter les redondants)
		Table tableOuterBean =outerBeanReflex.getBeanRep().getAnnotation(Table.class);
		
		//otenir le innerbean 
		BeanReflexRetrieve innerBeanrflx= new BeanReflexRetrieve(outerBeanField.getAnnotation(VMignorer.class).type(),tableOuterBean);
		this.innerBeanReflex = innerBeanrflx;
		
		//otenir le innerBeanField
		this.innerBeanField = innerBeanrflx.getBeanRep().getDeclaredField(outerBeanField.getAnnotation(VMignorer.class).innerBeanFieldName());
		
		return this.innerBeanReflex;
	}
	/*
	 * creer une methode pour obtenir le requete sql pour retirer les inner bean
	 */
	public String getInnerSql(){
		return annoField.where();
	}
	/*
	 * les getteur et setteur
	 */
	public BeanReflexRetrieve getOuterBeanReflex() {
		return outerBeanReflex;
	}
	public void setOuterBeanReflex(BeanReflexRetrieve outerBeanReflex) {
		this.outerBeanReflex = outerBeanReflex;
	}
	public Field getInnerBeanField() {
		return innerBeanField;
	}
	public void setInnerBeanField(Field innerBeanField) {
		this.innerBeanField = innerBeanField;
	}
	public Jointure getAnnoField() {
		return annoField;
	}
	public void setAnnoField(Jointure annoField) {
		this.annoField = annoField;
	}
	public Field getOuterBeanField() {
		return outerBeanField;
	}
	public void setOuterBeanField(Field outerBeanField) {
		this.outerBeanField = outerBeanField;
	}
	
	

}
