import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Guice;
import com.google.inject.Injector;

import bean.Cours;
import bean.Etudiant;
import bean.Inscription;
import patternProxy.PersistentManager;
import patternProxy.ProxyPM;
/*
 * creer une clas Client Class pour tester le PM
 */
public class ClientClass {
	public static void main(String[] args) throws Throwable {
		PersistentManager pm  = new ProxyPM();
		/*
		 * test la methode retrieveSet
		 */
		List x1 = pm.retrieveSet(Etudiant.class, "select * from etudiant");
		List x2 = pm.retrieveSet(Cours.class, "select * from cours");
		List x3 = pm.retrieveSet(Inscription.class, "select * from inscription");
		System.out.println("*****************list de etudiant:");
		x1.stream().forEach(e-> ((Etudiant) e).afficheEtudiant());
		System.out.println("************************List de cours:");
		x2.stream().forEach(e-> ((Cours) e).afficheCours());
		System.out.println("***********************List de inscription:");
		x3.stream().forEach(e-> System.out.println(e));
		/*
		 * creer les bean pour l'insertion
		 */
		Cours c1 = new Cours();c1.setCoursID(4);c1.setDescription("xxx");c1.setName("c1");c1.setSigle("c1");
		Cours c2 = new Cours();c2.setCoursID(5);c2.setDescription("xxx");c2.setName("c2");c2.setSigle("c2");
		Cours c3 = new Cours();c3.setCoursID(6);c3.setDescription("xxx");c3.setName("c3");c3.setSigle("c3");
		Etudiant e1 = new Etudiant();e1.setFname("e1");e1.setLname("e1");e1.setEtudiantID(4);e1.setAge(1);
		Etudiant e2 = new Etudiant();e2.setFname("e2");e2.setLname("e2");e2.setEtudiantID(5);e2.setAge(1);
		Etudiant e3 = new Etudiant();e3.setFname("e3");e3.setLname("e3");e3.setEtudiantID(6);e3.setAge(1);
		Etudiant e4 = new Etudiant();e4.setFname("e4");e4.setLname("e4");e4.setEtudiantID(7);e4.setAge(1);
		Inscription i1 = new Inscription();i1.setCoursID(4);i1.setEtudiantID(4);i1.setEtudiant(e1);i1.setCours(c1);//i1.setInscriptionID(7);
		Inscription i2 = new Inscription();i2.setCoursID(4);i2.setEtudiantID(5);i2.setEtudiant(e2);i2.setCours(c1);//i2.setInscriptionID(8);
		Inscription i3 = new Inscription();i3.setCoursID(4);i3.setEtudiantID(6);i3.setEtudiant(e3);i3.setCours(c1);//i3.setInscriptionID(9);
		Inscription i4 = new Inscription();i4.setCoursID(5);i4.setEtudiantID(4);i4.setEtudiant(e1);i4.setCours(c2);//i4.setInscriptionID(10);
		Inscription i5 = new Inscription();i5.setCoursID(5);i5.setEtudiantID(5);i5.setEtudiant(e2);i5.setCours(c2);//i5.setInscriptionID(11);
		Inscription i6 = new Inscription();i6.setCoursID(6);i6.setEtudiantID(6);i6.setEtudiant(e3);i6.setCours(c3);//i6.setInscriptionID(12);
		List<Inscription> l1 = new ArrayList();l1.add(i1);l1.add(i4);e1.setInscriptions(l1);
		List<Inscription> l2 = new ArrayList();l2.add(i2);l2.add(i5);e2.setInscriptions(l2);
		List<Inscription> l3 = new ArrayList();l3.add(i3);l3.add(i6);e3.setInscriptions(l3);
		List<Inscription> l4 = new ArrayList();l4.add(i1);l4.add(i2);l4.add(i3);c1.setInscriptions(l4);
		List<Inscription> l5 = new ArrayList();l5.add(i5);l5.add(i4);c2.setInscriptions(l5);
		List<Inscription> l6 = new ArrayList();l6.add(i6);c3.setInscriptions(l6);
		List<Etudiant> le = new ArrayList();List<Cours> lc = new ArrayList();
		le.add(e1);le.add(e2);le.add(e3);lc.add(c1);lc.add(c2);lc.add(c3);
		/*
		 * tester la methode insert et bulkInsert
		 */
		pm.insert(e2);Etudiant newEtudiant = new Etudiant(9,1, "abc", "adc", null);
        pm.insert(newEtudiant);
		pm.bulkInsert(le); 
		




	}



}
