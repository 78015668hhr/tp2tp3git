package bean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import annotation.AutoIncrement;
import annotation.Jointure;
import annotation.Table;
import annotation.VMignorer;

/*
 * creer une class bean etudiant 
 */
@Table(TableName = "cours",
clePrimaire	=	"coursID"
		)
public class Cours {
	@AutoIncrement(isF_key = true)
	private int coursID;
	
	private String name,sigle,description;
	
	@Jointure(where = Jointure.jointureSQL+"where cours.coursid = ",joinOn = "coursID" )
	@VMignorer(type = Inscription.class, innerBeanFieldName = "cours")
	private List<Inscription>inscriptions=null;
	
	/*
	 * les getter et setteur
	 */
	public int getCoursID() {
		return coursID;
	}
	
	public void setCoursID(int coursID) {
		this.coursID = coursID;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSigle() {
		return sigle;
	}
	
	public void setSigle(String sigle) {
		this.sigle = sigle;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<Inscription> getInscriptions() {
		return inscriptions;
	}
	
	public void setInscriptions(List<Inscription> inscriptions) {
		this.inscriptions = inscriptions;
	}
	
	/*
	 * defini une methode qui afficher tout les information d'un cours
	 */
	public void afficheCours() {
		System.out.println(sigle + " "+ name + " id: "+ coursID+" description: "+description);
		if(inscriptions!=null) {
			System.out.println("list de incription de cours "+name+" :");
			inscriptions.stream().forEach(System.out::println);
			}

	}

}
