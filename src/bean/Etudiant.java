package bean;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import annotation.AutoIncrement;
import annotation.Table;
import annotation.VMignorer;
import annotation.Jointure;
/*
 * creer une class bean etudiant 
 */
@Table(TableName = "etudiant",
clePrimaire	=	"etudiantID"
)
public class Etudiant {
	
	@AutoIncrement(isF_key = true)
	private int etudiantID;
	
	private int age;
	
	private String fname,lname;
	
	@Jointure(where = Jointure.jointureSQL+"where etudiant.etudiantid = ",joinOn = "etudiantID")
	@VMignorer(type = Inscription.class, innerBeanFieldName = "etudiant")
	private List<Inscription>inscriptions=null;
	
	//consctuctor 
	public Etudiant(int etudiantID, int age, String fname, String lname, List<Inscription> inscriptions) {
		//super();
		this.etudiantID = etudiantID;
		this.age = age;
		this.fname = fname;
		this.lname = lname;
		this.inscriptions = inscriptions;
	}
	//contructor par default
	public Etudiant() {
		
	}
	
	/*
	 * les getter et setter
	 */
	public int getEtudiantid() {
		return etudiantID;
	}
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public List<Inscription> getInscriptions() {
		return inscriptions;
	}
	public void setInscriptions(List<Inscription> inscriptions) {
		this.inscriptions = inscriptions;
	}
	public String toString() {
		
		//inscriptions.stream().forEach(e-> getString(e.toString()));
		return fname+" "+lname+" age: "+age+" id: "+etudiantID;
	}
	public void getString(String s) {
		//Stringins += s+"\n";
	}
	public int getEtudiantID() {
		return etudiantID;
	}
	public void setEtudiantID(int etudiantID) {
		this.etudiantID = etudiantID;
	}
	/*
	 * defini une methode qui afficher tout les information d'un cours
	 */
	public void afficheEtudiant() {
		System.out.println(fname+" "+lname+" age: "+age+" id: "+etudiantID);
		if(inscriptions!=null) {
			System.out.println("list de incription de "+lname+" :");
		inscriptions.stream().forEach(System.out::println);
		}
	}

	
	
	
	
}
