package bean;

import java.awt.List;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import annotation.AutoIncrement;
import annotation.Jointure;
import annotation.Table;
import annotation.VMignorer;

/*
 * creer une class de bean inscirption
 */
@Table(TableName = "inscription",
clePrimaire	=	"inscriptionID"
		)
public class Inscription {
	@AutoIncrement(isF_key = false)
	private int inscriptionID;
	
	private int coursID;
	
	private int etudiantID;
	
	@Jointure(where = Jointure.jointureSQL+"where inscription.inscriptionid = ",joinOn = "inscriptionID")
	@VMignorer(type = Cours.class, innerBeanFieldName = "inscriptions")
	private Cours cours;
	
	@Jointure(where = Jointure.jointureSQL+"where inscription.inscriptionid = ",joinOn = "inscriptionID")
	@VMignorer(type = Etudiant.class, innerBeanFieldName = "inscriptions")
	private Etudiant etudiant;
	
	/*
	 * les getteur est setteur
	 */
	public int getInscriptionID() {
		return inscriptionID;
	}
	public void setInscriptionID(int inscriptionID) {
		this.inscriptionID = inscriptionID;
	}
	public int getCoursID() {
		return coursID;
	}
	public void setCoursID(int coursID) {
		this.coursID = coursID;
	}
	public int getEtudiantID() {
		return etudiantID;
	}
	public void setEtudiantID(int etudiantID) {
		this.etudiantID = etudiantID;
	}
	public Cours getCours() {
		return cours;
	}
	public void setCours(Cours cours) {
		this.cours = cours;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	/*
	 * mothode toString
	 */
	public String toString() {
		return etudiant.getLname() + " incrit le cours:"+ cours.getName();
	}


}
