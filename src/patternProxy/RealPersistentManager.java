package patternProxy;
import java.lang.reflect.Field;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import BeanRflex.BeanReflexRetrieve;
import BeanRflex.ToMany;
import BeanRflex.ToOne;
import Exception.ErreurAccesAuxChamps;
import annotation.AutoIncrement;
import annotation.Table;
import annotation.Transaction;
/*
 * creer une class RealPersistentManager qui connecte une base donnee et les manipuler
 * qui est la class implementation qui implemente la class PersistentManager(pattern proxy)
 */
public class RealPersistentManager implements PersistentManager{
	private static final String URL_DATABASE = "jdbc:postgresql://localhost:5432/BDinscription";
	public static Connection connection;
	private final String STRING_TYPE = "java.lang.String";
	private final String INT_TYPE = "int";
	private final String LIST_TYPE = "java.util.List";
	private final String REQUEST_CREATE_TABLE_ETUDIANT =
			"CREATE TABLE ETUDIANT( "
					+ "ETUDIANTID SERIAL PRIMARY KEY, "
					+ "FNAME TEXT NOT NULL, "
					+ "LNAME TEXT NOT NULL, "
					+ "AGE INT NOT NULL " +
					" )";
	private final String REQUEST_CREATE_TABLE_COURS =
			"CREATE TABLE COURS( "
					+ "COURSID SERIAL PRIMARY KEY, "
					+ "NAME TEXT NOT NULL, "
					+ "SIGLE TEXT NOT NULL, "
					+ "DESCRIPTION TEXT NOT NULL " +
					" )";
	private final String REQUEST_CREATE_TABLE_INSCRIPTION =
			"CREATE TABLE INSCRIPTION( "
					+ "INSCRIPTIONID SERIAL PRIMARY KEY, "
					+ "COURSID INT REFERENCES COURS(COURSID) ON DELETE CASCADE, "
					+ "ETUDIANTID INT REFERENCES ETUDIANT(ETUDIANTID) ON DELETE CASCADE, "
					+ "DATE TIMESTAMP" +
					" )";

	private final String REQUEST_DELETE_ALL_ROW_ETUDIANT = "DELETE FROM ETUDIANT";
	private final String REQUEST_DROP_TABLE_ETUDIANT = "DROP TABLE IF EXISTS ETUDIANT";
	private final String REQUEST_DROP_TABLE_COURS = "DROP TABLE IF EXISTS COURS";
	private final String REQUEST_DROP_TABLE_INSCRIPTION = "DROP TABLE IF EXISTS INSCRIPTION";
	public static final String QUERY_GET_ALL_ETUDIANT = "SELECT * FROM ETUDIANT";
    /*
     * consctructor
     * connecter la base donne et inserer les bean predefini lors la creation de nouveau instance de pm
     */
	public RealPersistentManager() {
		connectionToDatabase();
		dropTables();
		createTables();
		insertData();
	}
	/*
	 * une implemente methode public pour la retirer les beans recursivement
	 */
	@Override
	public <T> List<T> retrieveSet(Class<T> tClass, String sql) {
		
		//obtenir l'instance de BeanReflexRetrieve pour la prochanie operation
		BeanReflexRetrieve beanRflex = new BeanReflexRetrieve(tClass,tClass.getAnnotation(Table.class));
		
		return retrieveSet2(beanRflex,sql);	//appeler la methode retrieveSet2
	}
	/*
	 * une methode private qui donne la main au methode retrieveSet
	 * il est la vraie methode qui traite la requete sql!!!
	 */
	private <T> List<T> retrieveSet2(BeanReflexRetrieve beanRflex,String sql){
		List<T> listTObject = null;
		T tObject = null;
		
		
		List<Field> fields = beanRflex.getListField();
		try {
			listTObject = new ArrayList<>();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql); 
			
			//dans chaque iteration de la boucle while, retirer tout les fields simple et les fields complex
			while (resultSet.next()) {
				tObject = (T) beanRflex.getBeanRep().newInstance();
				
				//dans la boucle for,retirer tout les fields de type simple
				for(Field fieldT : fields) {
					fieldT.setAccessible(true);
					String nameField = fieldT.getName();
					String typeField = fieldT.getType().getTypeName();
					switch (typeField) {
					case STRING_TYPE:
						fieldT.set(tObject, resultSet.getString(nameField));
						break;
					case INT_TYPE:
						fieldT.setInt(tObject, resultSet.getInt(nameField));
						break;
					default:
						break;
					} // end switch                            
				} //end for
				
				//appeler la methode retrieveTypeComplexField pour retirer les field de type bean ou liste de bean
				retrieveTypeComplexField(tObject,beanRflex);
				listTObject.add(tObject);	
			}//end while
		}catch (InstantiationException | SQLException | IllegalAccessException | SecurityException e) {
			e.printStackTrace();
		}   
		return listTObject;
	}
	/*
	 * une methode private pour retirer les field de type bean ou liste de bean
	 */
	private void retrieveTypeComplexField(Object outerBean,BeanReflexRetrieve beanRflex){
		List<ToMany> toManys = beanRflex.getListToMany();
		List<ToOne> toOnes = beanRflex.getListToOne();
		try {
			
			//si le field complex est type de la liste de bean
			if(!toManys.isEmpty()) {
				
				for(ToMany tm: toManys) {
					//obtenir la field,qui ont la valeur specifique,sert a composer la requete sql
					//par exemple : la valeur a droite de sql: select * from (cours inner join inscription on cours.coursid = inscription.coursid) 
					//inner join etudiant on etudiant.etudiantid = inscription.etudiantid  where etudiantid = ........
					Field fieldJoin = beanRflex.getBeanRep().getDeclaredField(tm.getAnnoField().joinOn());fieldJoin.setAccessible(true);
					
					//obtenir la valeur du field de la type liste de bean, appler recursivement la methode retrieveSet2
					List listCollection = retrieveSet2(tm.getInnerBeanReflex(),tm.getInnerSql()+fieldJoin.get(outerBean)+";");
					
					//obtenir la field de la type liste de bean de la outer bean(le bean que on va retirer)
					Field outerBeanField = tm.getOuterBeanField();outerBeanField.setAccessible(true);
					
					//set la valeur a outer bean
					outerBeanField.set(outerBean, listCollection);
					
					for(Object t :listCollection) {
						
						//obtenir la field complex de inner bean
						tm.getInnerBeanField().setAccessible(true);
						
						//set outer bean a inner bean field
						tm.getInnerBeanField().set(t, outerBean);
						
						}//end for
	              
					//apres ces code , c'est une bidirectif(deux sens) reference entre outer bean et inner bean
				}
				
			}
			//si le field complex est la type bean
			if(!toOnes.isEmpty()) {
				
				for(ToOne to: toOnes) {
					//obtenir la field,qui ont la valeur specifique,sert a composer la requete sql
					Field fieldJoin = beanRflex.getBeanRep().getDeclaredField(to.getAnnoField().joinOn());fieldJoin.setAccessible(true);
					
					//obtenir la valeur du field de la type liste de bean, appler recursivement la methode retrieveSet2
					List listCollection = retrieveSet2(to.getInnerBeanReflex(),to.getInnerSql()+fieldJoin.get(outerBean)+";");
					
					//parce que le field complex est la type bean,donc c'est sur que il y a une seul object dans la liste 
					Object toOneObject= listCollection.get(0);
					
					//obtenir la field de la type liste de bean de la outer bean(le bean que on va retirer)
					Field OuterBeanField =to.getOuterBeanField();OuterBeanField.setAccessible(true);
					
					//set la valeur a outer bean
					OuterBeanField.set(outerBean,toOneObject);
					
					//obtenir la field complex de inner bean
					Field innerBeanField=to.getInnerBeanField();innerBeanField.setAccessible(true);
					
					
					List listObject = new ArrayList();listObject.add(outerBean);
					
					//set outer bean a inner bean field
					innerBeanField.set(toOneObject, listObject);
				}//end for
				
			}
			
			}catch( IllegalAccessException | NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
	}
	/*
	 * une implemente methode public pour insert une liste de bean dans plusieurs rangee de base donne recursivement
	 * avec la annotation "transaction" comme une marque pour la matcher les trouver
	 */
	@Override
	@Transaction
	public <T>	int	 bulkInsert(List<T>	b) throws Throwable {
		T t = b.get(0);
		Class<T> tClass = (Class<T>) t.getClass();Table annoTable = tClass.getAnnotation(Table.class);
		
		//obtenir l'instance de BeanReflexRetrieve pour la prochanie operation
		BeanReflexRetrieve beanRflex = new BeanReflexRetrieve(tClass,annoTable);
		
		for(T tObject : b)
			insert2(tObject,beanRflex);//appeler la methode insert2 dans la boucle for pour inserer chaque beans
		
		return b.size();
	}
	/*
	 * une implemente methode public pour insert une bean dans le base donne recursivement
	 * avec la annotation "transaction" comme une marque pour la matcher les trouver
	 */
	@Override
	@Transaction
	public  <T> int insert(T b) throws Throwable  {
		Class<T> tClass = (Class<T>) b.getClass();Table annoTable = tClass.getAnnotation(Table.class);
		
		//obtenir l'instance de BeanReflexRetrieve pour la prochanie operation
		BeanReflexRetrieve beanRflex = new BeanReflexRetrieve(tClass,annoTable);
		
		//appeler la methode insert2
		insert2(b,beanRflex);
		
		return 1;
	}
	/*
	 * une methode private qui generer la requete sql pour l'insertion
	 */
	public <T>void insert2(T b,BeanReflexRetrieve beanRflex)  throws Throwable{
		Class<T> tClass = (Class<T>) b.getClass();Table annoTable = tClass.getAnnotation(Table.class);
		
		//obtenir la table name dans sql
		String tableName = annoTable.TableName();
		
		//obtenir la primary key name dans sql
		String keyName = annoTable.clePrimaire();

		List<Field> fields = beanRflex.getListField();
		
			Field keyField = tClass.getDeclaredField(keyName);keyField.setAccessible(true);
			
			if(!keyField.isAccessible())//si la keyField n'est pas accessible ,alors lancer l'exception personalise "ErreurAccesAuxChamps"!!!
				throw new ErreurAccesAuxChamps(keyField);
			
			//pour evider les redondants, supprimer les inscriptions est deja existe 
			Statement statement = connection.createStatement();
			statement.executeUpdate("DELETE FROM " + tableName+" WHERE "+ keyName +" = "+keyField.get(b));
			
			//decalrer une variable string pour presenter la premier parti de sql de l'insertion
			String sqlInsertToTable = "INSERT INTO "+tableName + " (";
			
			//decalrer une variable string pour presenter la deuxieme parti de sql de l'insertion
			String sqlInsertValues = "VALUES(";
			
			//dans le boucle for ,parcourir tout les field de type simple ,et completer le requete sql
			for (Field f : fields) { 
				f.setAccessible(true);
				
				if(!f.isAccessible())//si la field n'est pas accessible ,alors lancer l'exception personalise "ErreurAccesAuxChamps"!!!
					throw new ErreurAccesAuxChamps(f);
				
				String typeField = f.getType().getTypeName();
				
				if(f.isAnnotationPresent(AutoIncrement.class)&&!f.getAnnotation(AutoIncrement.class).isF_key()) 		
					continue;//si la field n'a pas l'annotation "AutoIncrement",on va interdir increment auomatiquement!!!!!!
				
				if(typeField.equals(INT_TYPE)) {
					sqlInsertToTable += f.getName()+",";
					sqlInsertValues += f.get(b)+",";}
				else if(typeField.equals(STRING_TYPE)) {
					sqlInsertToTable += f.getName()+",";
					sqlInsertValues += "'"+f.get(b)+"'"+",";}	
				else
					continue;
			}// end for
			
			sqlInsertToTable = sqlInsertToTable.substring(0, sqlInsertToTable.length()-1)+") ";
			sqlInsertValues = sqlInsertValues.substring(0, sqlInsertValues.length()-1)+");";
			
			//obtenir la requete sql complet!
			String insertsql = sqlInsertToTable+sqlInsertValues;
			
			//appeler la methode insertBeanComplex pour inserer les field de type complex
			insertBeanComplex(b, beanRflex, statement, insertsql);	

	}
	/*
	 * une methode private qui inserer les field de type complex
	 * il est le vraie methode qui executer le requete sql aussi!
	 */
	private void insertBeanComplex(Object b,BeanReflexRetrieve beanRflex,Statement statement,String insertsql)throws Throwable{
		List<ToMany> toManys = beanRflex.getListToMany();
		List<ToOne> toOnes = beanRflex.getListToOne();
		
        //si le field complex est le type de bean
		if(!toOnes.isEmpty()) {
			for(ToOne to: toOnes) {
				
				//defini une variable boolean hasCascade pour identifier si le bean est existe deja dans le base donne,
				//parceque il a des champ qui est type de bean ,c'est sur qu'il a une cascade avec l'autre table
				//par exmemple , une inscription ont des cle etranger coursid et etudiantid,si on le suprrimer , on doit suprrimer les cours et 
				// les etudiant qui ont la meme key de  la meme valeur, on doit eviter ca!!!
				boolean existe = false;
				
				Field OuterBeanField = to.getOuterBeanField();
				OuterBeanField.setAccessible(true);
				
				//si le OuterBeanField n'est pas accesible,on lance l'exception ErreurAccesAuxChamps personalise 
				if(!OuterBeanField.isAccessible())
					throw new ErreurAccesAuxChamps(OuterBeanField);
				
				String tableName = OuterBeanField.get(b).getClass().getAnnotation(Table.class).TableName();
				String pKeyName = OuterBeanField.get(b).getClass().getAnnotation(Table.class).clePrimaire();
				ResultSet resultSet = statement.executeQuery("select "+pKeyName+" from "+tableName);  
				Field f =OuterBeanField.get(b).getClass().getDeclaredField(pKeyName);f.setAccessible(true);
				
				//si le OuterBeanField n'est pas accesible,on lance l'exception ErreurAccesAuxChamps personalise 
				if(!f.isAccessible())
					throw new ErreurAccesAuxChamps(f);
				
				//dans le boucle while on tester si l'inscription qui presnte par le bean est deja dans la table
				while(resultSet.next()) {
					
					//si on trouve il est existe deja !!!
					if(resultSet.getInt(pKeyName)==(int)f.get(OuterBeanField.get(b))) {
						existe = true;
						break;
					}
					
				}
				
				//quand on a les inscription existe deja, on saute la boucle
				if(existe)
					continue;
				
				//appeler la methode insert2 recursivement pour inserer le innerbean
				insert2(OuterBeanField.get(b),to.getInnerBeanReflex());
			}//terminer la boucle while 
			
			//parceque !toOnes.isEmpty() == true, ca veut dire que il ya des cascade , on doit d'abord inserer ses innerbean
			//apres on peut excute le sql dessous!!
			statement.executeUpdate(insertsql);
		}
		//parceque !toOnes.isEmpty() == false, ca veut dire que il y a  pas de cascade , on doit d'abord excute le sql dessous!!
		//apres on peut inserer ses innerbean!!
		else 
			statement.executeUpdate(insertsql);
        
		//si le field complex est le type de la liste de bean
		if(!toManys.isEmpty()){
			for(ToMany tm: toManys) {
				Field OuterBeanField = tm.getOuterBeanField();OuterBeanField.setAccessible(true);
				
				//si le OuterBeanField n'est pas accesible,on lance l'exception ErreurAccesAuxChamps personalise 
				if(!OuterBeanField.isAccessible())
					throw new ErreurAccesAuxChamps(OuterBeanField);
				
				//si le liste de bean n'est pas null
				if((List)OuterBeanField.get(b)!=null)
				
				//appeler la methode insert2 recursivement pour inserer le innerbean dans la boucle for
				for(Object innerBean : (List)OuterBeanField.get(b))
					insert2(innerBean,tm.getInnerBeanReflex());//end for
				
			}//end outer for 
		}
	
	}
	/*
	 * une methode prviate pour connecter le BD
	 */
	private void connectionToDatabase() {
		try {
			//TODO mettre vos identifiant de connection a la base de donnees ici
			connection = DriverManager.getConnection(URL_DATABASE, "postgres", "");
			System.out.println("connexion a la base donnees reussie");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * une metode private qui creer les table
	 */
	private void createTables() {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(REQUEST_CREATE_TABLE_ETUDIANT);
			statement.executeUpdate(REQUEST_CREATE_TABLE_COURS);
			statement.executeUpdate(REQUEST_CREATE_TABLE_INSCRIPTION);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * une methode qui suprrimer les table 
	 */
	private void dropTables() {
		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(REQUEST_DROP_TABLE_INSCRIPTION);
			statement.executeUpdate(REQUEST_DROP_TABLE_ETUDIANT);
			statement.executeUpdate(REQUEST_DROP_TABLE_COURS);


		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * une methode qui inserer les data predefini
	 */
	private void insertData() {
		try {
			Statement statement = connection.createStatement();
			String insert1 = String.format("INSERT INTO ETUDIANT (FNAME, LNAME, AGE) VALUES ('%s', '%s' , %d)", "hanrui", "huang", 20);
			String insert2 = String.format("INSERT INTO ETUDIANT (FNAME, LNAME, AGE) VALUES ('%s', '%s', %d)", "wang", "tao", 25);
			String insert3 = String.format("INSERT INTO ETUDIANT (FNAME, LNAME, AGE) VALUES ('%s', '%s', %d)", "fadel", "toure", 27);
			statement.executeUpdate(insert1);
			statement.executeUpdate(insert2);
			statement.executeUpdate(insert3);
			String insert4 = String.format("INSERT INTO COURS (NAME, SIGLE, DESCRIPTION) VALUES ('%s', '%s' , '%s')", "concept oo", "INF1035", "xxxxx");
			String insert5 = String.format("INSERT INTO COURS (NAME, SIGLE, DESCRIPTION) VALUES ('%s', '%s','%s')", "base de donne", "STT1001", "yyyyyy");
			String insert6 = String.format("INSERT INTO COURS (NAME, SIGLE, DESCRIPTION) VALUES ('%s', '%s', '%s')", "mathematique de informatqiue", "PIF1001", "zzzzzz");
			statement.executeUpdate(insert4);
			statement.executeUpdate(insert5);
			statement.executeUpdate(insert6);
			String insert7 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 1, 1);
			String insert8 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 2, 1);
			String insert9 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 3, 1);
			String insert10 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 1, 2);
			String insert11 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 2, 2);
			String insert12 = String.format("INSERT INTO INSCRIPTION (COURSID, ETUDIANTID, DATE) VALUES (%d, %d, '2019-01-01 00:00:01')", 1, 3);
			statement.executeUpdate(insert7);
			statement.executeUpdate(insert8);
			statement.executeUpdate(insert9);
			statement.executeUpdate(insert10);
			statement.executeUpdate(insert11);
			statement.executeUpdate(insert12);
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}



}
