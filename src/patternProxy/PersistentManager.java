package patternProxy;
import java.util.List;
/*
 * creer une interface PersistentManager qui defini les comportement de PM(pattern proxy)
 */
public interface PersistentManager {
	public <T> List<T> retrieveSet(Class<T> tClass, String sql);
	public <T>	int	 bulkInsert(List<T>	b) throws Throwable;
	public  <T> int insert(T b) throws Throwable;

}
