package patternProxy;
import java.util.List;

import com.google.inject.Guice;
import com.google.inject.Injector;

import annotation.Transaction;
import aop.TransactionModule;
/*
 * creer une class ProxyPM qui est la class proxy de la class RealPersistentManager(qui utilise RealPersistentManager)
 * qui est la class implementation qui implemente la class PersistentManager(pattern proxy)
 */
public class ProxyPM implements PersistentManager{
    private RealPersistentManager pm;
    
    /*
     * consctructor
     */
	public ProxyPM() {
		Injector injector = Guice.createInjector(new TransactionModule());
		pm = injector.getInstance(RealPersistentManager.class);
	}
	
    /*
     * les methode override ,qui utilise la class RealPersistentManager
     */
	@Override
	public <T> List<T> retrieveSet(Class<T> tClass, String sql) {
		return pm.retrieveSet(tClass, sql);
	}

	@Override
	public <T> int bulkInsert(List<T> b) throws Throwable {
		return pm.bulkInsert(b);
	}

	@Override
	public <T> int insert(T b) throws Throwable {
		return pm.insert(b);	
	}
	
}
