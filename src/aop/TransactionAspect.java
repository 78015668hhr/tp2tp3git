package aop;

import java.sql.Connection;
import java.sql.SQLException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import patternProxy.RealPersistentManager;
/*
 * defini une class qui realiser l'aspect dans la notion aop,pour intercepter la mothode!!!
 * il a implementer la class MethodInterceptor dans guice
 */
public class TransactionAspect implements MethodInterceptor{
	private Connection con;
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable{
		
		try {
			//obtenir la connection dans Pm
			con = ((RealPersistentManager)invocation.getThis()).connection;
			
			//empecher la connection excute le sql automatiquement
			con.setAutoCommit(false);
			
			//invoke la moethode
			Object a = invocation.proceed();
			//invocation.getArguments()
			//con.commit();
			
			//si y a pas de exception, fixe connection excute sql automatiquement
			con.setAutoCommit(true);
			
			System.out.println("enregirstrer les beans avec succes!!!!!!!!!!");
			return a;
		}catch(Throwable e) {//si y a n'importe quel exception
            e.printStackTrace();
			System.out.println("une exception apparait,toute la transaction	est annul�e!!!!!!");
			
			//rollback tout les operations!!!
			con.rollback();
			
			//reset auto commit == true !!!
			con.setAutoCommit(true);


		}
		
		return null;
		


	}

}
