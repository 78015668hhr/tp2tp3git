package aop;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

import annotation.Transaction;
/*
 * creer une class qui implement la class AbstractModule dans guice 
 */
public class TransactionModule extends AbstractModule{
	
	/*
	 * override la methode configure pour la matcher chercher les methode qui a l'annotation "transaction"
	 */
	@Override
    protected void configure() {
      
		TransactionAspect ap =new TransactionAspect();
        bindInterceptor(Matchers.any(),
                        Matchers.annotatedWith(Transaction.class),
                        ap);
        bindInterceptor(Matchers.any(),
                Matchers.annotatedWith(Transaction.class),
                ap);

    }

}
